# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: fdubois <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/06/03 10:41:01 by fdubois           #+#    #+#              #
#    Updated: 2019/07/15 17:06:34 by almoraru         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc

SRC_DIR = srcs

OBJ_DIR = srcs

COLOR = \x1b[1;34m

COLOR2 = \x1b[1;35m

COLOR3 = \x1b[0;32m

COLOR4 = \x1b[0;36m

COLOREND = \033[0;0m

SRCS = $(SRC_DIR)/main.c $(SRC_DIR)/errors.c $(SRC_DIR)/png.c			 	\
$(SRC_DIR)/png_filters.c $(SRC_DIR)/png_unzip.c $(SRC_DIR)/png_utils.c	 	\
$(SRC_DIR)/png_utils2.c  $(SRC_DIR)/parse.c $(SRC_DIR)/init.c			 	\
$(SRC_DIR)/slav3d.c $(SRC_DIR)/game.c $(SRC_DIR)/sdl_events.c				\
$(SRC_DIR)/ray_caster.c $(SRC_DIR)/sort.c $(SRC_DIR)/parse_utils.c		 	\
$(SRC_DIR)/floor_casting.c $(SRC_DIR)/wall_casting.c					 	\
$(SRC_DIR)/sprite_casting.c $(SRC_DIR)/parse_list.c $(SRC_DIR)/png_utils3.c \
$(SRC_DIR)/move.c $(SRC_DIR)/placement.c $(SRC_DIR)/free_utils.c			\
$(SRC_DIR)/put_sprite_tex.c $(SRC_DIR)/audio.c

NAME = wolf3d

FLAGS = -Wall -Wextra -Werror -g #-fsanitize=address

OBJECTS = $(SRCS:%.c=%.o)

LIB_PATH = libft/

SDL_LIB = ~/.brew/Cellar/sdl2/2.0.9_1/lib/

HEADER_PATH = includes/

INCLUDES = $(HEADER_PATH)/

INCLUDE = -I$(LIB_PATH)/ -I$(HEADER_PATH)/

all: $(NAME)

$(OBJECTS): $(OBJ_DIR)/%.o : $(SRC_DIR)/%.c $(INCLUDES)
	@$(CC) `sdl2-config --cflags` -c $< -o $@ $(FLAGS) $(INCLUDE)

$(NAME): $(OBJECTS)
	@make -C libft
	@$(CC) -o $@ $^ $(FLAGS) -I includes/SDL2/ -L libft/ -lft $(INCLUDE) \
	-L $(SDL_LIB) `sdl2-config --libs` -lSDL2 -lSDL2_mixer -lz
	@echo "$(COLOR)Wolf3d : Blyat Royale has been successfully installed$(COLOREND)"
clean:
	@echo "$(COLOR3)Cleaning objects!$(COLOREND)"
	@make clean -C libft
	@rm -f $(OBJECTS)

fclean: clean
	@echo "$(COLOR)Removing :("
	@make fclean -C libft
	@rm -f $(NAME)
	@echo "$(COLOR4)You murderer!$(COLOREND)"

re: fclean all

.PHONY: all clean fclean re
