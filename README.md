**Slav 3d**

Currently:
- Works on Linux/MacOS (Makefile needs to be changed for LINUX)
- textures on walls
- textures on floor/ceiling
- simple UI
- sprites that follow the camera
- png reader(kinda)

TODO:
- add a proper Makefile for both OS
- add hitscan
- add more enemies
- add a proper UI
- add a small story mode

I will not return to this project later.

![](images/example00.png)
![](images/example01.png)
![](images/example02.png)