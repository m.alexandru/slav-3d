/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf3d.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/05 16:56:09 by fdubois           #+#    #+#             */
/*   Updated: 2019/07/15 17:00:27 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF3D_H
# define WOLF3D_H

# include "../libft/libft.h"
# include "png.h"
# include <math.h>
# include <SDL2/SDL.h>
# include <SDL2/SDL_audio.h>
# include <SDL2/SDL_mixer.h>

# define WIN_WIDTH 1024
# define WIN_HEIGHT 800

/*
**	Utils
*/

# define TRUE				1
# define FALSE				0
# define EPS				0.00000001
# define NB_TEXTURES		6
# define NB_SPRITES			42
# define NB_CHANNELS		64
# define U_DIV				1
# define V_DIV				1
# define PI					3.14159265359
# define V_MOVE				1
# define TEXTURE_ADDR		0x55AA
# define NORTH				2
# define EAST				0
# define SOUTH				3
# define WEST				1
# define MAX_SPEED			0.21f

# define UI_NB				2
# define HARDBASS_RADIUS	80
# define GOPNIK_RADIUS		21
# define CH_HARDBASS		42
# define CH_BLYAT			51
# define CH_BLYAT2			52

/*
**	MAP DEFINES
*/

# define MAP_CHARS_LABEL	"ABCDEFGHIJPRSTUVWXYZ"
# define MAP_PICS_LABEL		"BCDERSTU"
# define MAP_SPRITES_LABEL	"AGHIJVXYZ"

# define P					-1
# define F					0
# define W					1
# define A					2
# define B					3
# define C					4
# define D					5
# define E					6
# define G					7
# define H					8
# define I					9
# define J					10
# define R					11
# define S					12
# define T					13
# define U					14
# define V					15
# define X					16
# define Y					17
# define Z					18

/*
**	Music and Sound Effects
*/

# define NB_BGM				1
# define NB_SFX				11

/*
**	Player Movements (Bitfield)
*/

# define M_F				1
# define M_B				(1 << 1)
# define M_L				(1 << 2)
# define M_R				(1 << 3)
# define M_SL				(1 << 4)
# define M_SR				(1 << 5)
# define M_RUN				(1 << 6)

/*
**	Inventory Defines
*/

# define I_AKM				1
# define I_VOD				(1 << 1)
# define I_RA1				(1 << 2)
# define I_RA2				(1 << 3)

/*
**	Sprites Defines
*/

# define S_PNJ				1
# define S_MUS				(1 << 1)
# define S_CHK				(1 << 2)
# define S_VOD				(1 << 3)
# define S_AKM				(1 << 4)
# define S_RA1				(1 << 5)
# define S_RA2				(1 << 6)

typedef struct	s_sprite
{
	Uint32	*texture;
	double	x;
	double	y;
	uint8_t	flags;
}				t_sprite;

typedef struct	s_texture
{
	void				*pixel_data;
	SDL_Texture			*texture;
	struct s_texture	*next;
	struct s_texture	*prev;
	Uint32				pitch;
	Uint16				tag;
}				t_texture;

typedef struct	s_game
{
	long	game_ticks;
	long	time;
	int		game;
}				t_game;

typedef struct	s_plane
{
	double	x;
	double	y;
}				t_plane;

typedef struct	s_pos
{
	double	x;
	double	y;
}				t_pos;

typedef struct	s_map
{
	signed char	**m;
	uint32_t	w;
	uint32_t	h;
}				t_map;

typedef struct	s_li
{
	struct s_li *next;
	signed char	*str;
}				t_li;

typedef struct	s_cam
{
	double	x;
	double	dir_x;
	double	dir_y;
}				t_cam;

typedef struct	s_ray
{
	double	dir_x;
	double	dir_y;
	double	side_dist_x;
	double	side_dist_y;
	double	delta_dist_x;
	double	delta_dist_y;
	int		hit;
	int		step_x;
	int		step_y;
}				t_ray;

typedef struct	s_map_pos
{
	int	x;
	int	y;
}				t_map_pos;

typedef struct	s_wall
{
	int		side;
	int		tex_x;
	int		tex_y;
	int		d;
	int		line_height;
	int		d_start;
	int		d_end;
	Uint32	color;
	double	x;
}				t_wall;

typedef struct	s_floor
{
	double	x;
	double	y;
	double	weight;
	double	x_wall;
	double	y_wall;
	int		tex_x;
	int		tex_y;
}				t_floor;

typedef struct	s_sprite_utils
{
	double	x;
	double	y;
	double	trans_x;
	double	trans_y;
	double	inv_det;
	double	v_move;
	int		height;
	int		width;
	int		d;
	int		screen_x;
	int		v_move_screen;
	int		u_div;
	int		v_div;
	int		d_start_x;
	int		d_end_x;
	int		d_start_y;
	int		d_end_y;
	int		tex_x;
	int		tex_y;
}				t_sprite_utils;

typedef struct	s_env
{
	signed char		**map;
	Mix_Music		*bgm[NB_BGM];
	Mix_Chunk		*sfx[NB_SFX];
	SDL_Surface		*surface;
	SDL_Surface		*textures[NB_TEXTURES];
	SDL_Surface		*sprite_tex[NB_SPRITES];
	SDL_Surface		*floor_tex[2];
	SDL_Window		*win;
	SDL_Texture		*ui[2];
	SDL_Event		event;
	SDL_Renderer	*renderer;
	Uint32			*screen_buf;
	t_texture		*managed_text;
	t_sprite		s[NB_SPRITES];
	double			z_buf[WIN_WIDTH];
	double			sprite_dist[NB_SPRITES];
	double			speed;
	int				sprite_order[NB_SPRITES];
	double			perp_dist;
	double			dist_wall;
	double			dist_player;
	double			current_dist;
	int				maph;
	int				mapw;
	int				sprite_nb;
	int				tex_num;
	int				inv;
	int				vodka;
	int				acc;
	uint32_t		hitscam;
	t_sprite_utils	sp;
	t_game			g;
	t_pos			pos;
	t_plane			plane;
	t_cam			cam;
	t_ray			ray;
	t_map_pos		m;
	t_floor			f;
	t_wall			wall;
	uint8_t			mv;
}				t_env;

/*
** Init functions
*/

int				ft_init(t_env *e);
int				init_win(char *title, unsigned int width, unsigned int height
								, t_env *e);
void			init_textures(t_env *e);
void			init_audio(t_env *e);

/*
** Parsing functinons
*/

void			parse(t_env *e, int ac, char **av);
t_li			*new_node(signed char *str);
void			add_node(t_li **list, t_li *node);
void			free_node(t_li **node);
int				list_size(t_li *list);
void			free_list(t_li **list, size_t len);
void			get_map_value(signed char *s);
int				ft_stris(signed char *str, char c);
void			ft_error_parse(char *s, t_env *e, int y, int x);
void			free_map(t_env *e, int max_y, int max_x);
void			free_everything_and_quit(t_env *e);
void			free_everything_and_quit_normal(t_env *e);
void			free_textures(t_env *e);
void			free_sdl(t_env *e);
void			ft_error_line(char *s, t_env *e, char *line);

/*
** PNG Parsing
*/

SDL_Surface		*png2surf(char *path);

/*
** Movement Functions
*/

void			move_forward_or_backward(t_env *e, int i);
void			move_right_or_left(t_env *e, double old_dir_x
								, double old_plane_x, int i);
void			move_it(t_env *e);
void			actual_move(t_env *e, int acc);
void			new_mvmt(t_env *e);
int				is_mvmt_key(int key);

/*
** Slav functions
*/

int				put_slavs(t_env *e, int i, int x, int y);
int				put_sprite_tex(t_env *e, int i, int x, int y);
void			*create_texture(unsigned int width, unsigned int height
								, t_env *e);
void			slav_draw_full_screen_texture(t_env *e, void *texture);
int				destroy_texture(void *p, t_env *e);

/*
** Audio functions
*/

int				get_hardbass_sprite_nb(t_env *e);
int				get_gopnik_sprite_nb(t_env *e);
void			hardbass_blyat(t_env *e);
void			angry_blyat(t_env *e);
void			play_bgm(t_env *e);

/*
** Raycasting functions
*/

void			calculate_initial_side_dist(t_env *e);
void			extend_ray_until_hit(t_env *e);
void			ray_cast(t_env *e);
void			prepare_floor_casting(t_env *e);
void			prep_and_avoid_over_flow(t_env *e);
void			draw_floor(t_env *e, int x);
void			prepare_draw(t_env *e);
void			prepare_draw_2(t_env *e);
void			draw_walls(t_env *e, int x);
void			sort_and_prep_sprites(t_env *e);
void			draw_sprites_height(t_env *e, int i);
void			draw_sprites_width(t_env *e, int i);
void			draw_sprites(t_env *e, int stripe, int i);

/*
** Util functions
*/

void			ft_error(char *s, t_env *e);
void			run_game(t_env *e);
void			handle_sdl_events(t_env *e);
void			free_before_quit(t_env *e);
void			comb_sort(int *order, double *dist, int amount);
int				skip_turtles(int gap);
void			sprite_swap(int *a, int *b);
void			sprite_swap2(double *a, double *b);
void			get_nb_sprites(t_env *e);
void			get_sprite_pos(t_env *e);
void			check_last_wall(t_env *e);
int				put_sprite_tex(t_env *e, int i, int x, int y);
int				put_sprite_tex1(t_env *e, int i, int x, int y);
int				put_sprite_tex2(t_env *e, int i, int x, int y);
int				put_sprite_tex3(t_env *e, int i, int x, int y);
int				put_sprite_tex4(t_env *e, int i, int x, int y);
void			free_audio(t_env *e);
void			free_sdl(t_env *e);
void			free_textures(t_env *e);

#endif
