/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 15:42:11 by almoraru          #+#    #+#             */
/*   Updated: 2018/11/10 18:15:37 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, const void *src, int c,
					size_t n)
{
	size_t				i;
	unsigned char		*dest;
	unsigned char		*srs;

	i = 0;
	dest = (unsigned char*)dst;
	srs = (unsigned char*)src;
	c = (unsigned char)c;
	while (i < n)
	{
		if ((*dest++ = *srs++) == c)
			return (dest);
		i++;
	}
	return (NULL);
}
