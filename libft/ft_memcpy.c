/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 18:09:17 by almoraru          #+#    #+#             */
/*   Updated: 2018/11/10 18:08:48 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	size_t		i;
	char		*dest;
	char		*srs;

	i = 0;
	dest = (char*)dst;
	srs = (char*)src;
	while (i < n)
	{
		dest[i] = srs[i];
		i++;
	}
	return (dest);
}
