/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_memory.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/26 18:43:00 by almoraru          #+#    #+#             */
/*   Updated: 2018/11/26 21:40:42 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void		ft_put_addr(unsigned long nb)
{
	unsigned char	addr[13];
	char			*hex;
	int				i;

	hex = "0123456789abcdef";
	i = 0;
	while (i < 12)
	{
		addr[i] = hex[nb % 16];
		nb /= 16;
		i++;
	}
	i--;
	ft_putstr("0x");
	while (i >= 0)
	{
		ft_putchar(addr[i]);
		i--;
	}
	ft_putchar(':');
	ft_putchar(' ');
}

static void		ft_put_mem(unsigned char c)
{
	char	*hex;

	hex = "0123456789ABCDEF";
	ft_putchar(hex[c / 16]);
	ft_putchar(hex[c % 16]);
}

static void		ft_addmem(unsigned char *mem, unsigned int size, unsigned int i)
{
	unsigned int	j;
	unsigned long	nb;

	nb = (unsigned long)mem;
	ft_put_addr(nb);
	j = 0;
	while (j < 16)
	{
		if (i * 16 + j < size)
			ft_put_mem(mem[j]);
		else
			ft_putstr("  ");
		if (j && (j + 1) % 2 == 0)
			ft_putchar(' ');
		j++;
	}
}

void			*ft_print_memory(void *addr, unsigned int size)
{
	unsigned char		*mem;
	unsigned int		i;
	unsigned int		j;

	mem = (unsigned char *)addr;
	i = 0;
	while (i * 16 < size)
	{
		ft_addmem(mem, size, i);
		ft_putchar(' ');
		j = 0;
		while (j < 16)
		{
			if (ft_isprint(mem[j]) && i * 16 + j < size)
				ft_putchar(mem[j]);
			else if (i * 16 + j < size)
				ft_putchar('.');
			j++;
		}
		ft_putchar('\n');
		mem += 16;
		i++;
	}
	return (addr);
}
