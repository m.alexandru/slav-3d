/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   audio.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/23 18:54:48 by fdubois           #+#    #+#             */
/*   Updated: 2019/06/23 19:16:23 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/wolf3d.h"

void	play_bgm(t_env *e)
{
	Mix_VolumeMusic(5);
	Mix_PlayMusic(e->bgm[0], -1);
}

int		get_hardbass_sprite_nb(t_env *e)
{
	int	i;

	i = e->sprite_nb;
	while (--i >= 0 && e->sprite_dist[i] < HARDBASS_RADIUS)
		if (e->s[e->sprite_order[i]].flags & S_RA1
		|| e->s[e->sprite_order[i]].flags & S_RA2)
			return (i);
	return (-1);
}

int		get_gopnik_sprite_nb(t_env *e)
{
	int	i;

	i = e->sprite_nb;
	while (--i >= 0 && e->sprite_dist[i] < GOPNIK_RADIUS)
		if (e->s[e->sprite_order[i]].flags & S_PNJ)
			return (i);
	return (-1);
}

void	angry_blyat(t_env *e)
{
	int	gopnik;

	gopnik = get_gopnik_sprite_nb(e);
	if (gopnik < 0)
		Mix_HaltChannel(CH_BLYAT);
	else
	{
		if (e->sprite_dist[gopnik] > 0.01f)
			Mix_SetPosition(CH_BLYAT, 0, (((double)(e->sprite_dist[gopnik]
			/ GOPNIK_RADIUS)) * 255));
		if (!Mix_Playing(CH_BLYAT) && e->sprite_dist[gopnik] < GOPNIK_RADIUS)
			Mix_PlayChannel(CH_BLYAT, e->sfx[((SDL_GetTicks() % 4) + 5)]
			, -1);
	}
}

void	hardbass_blyat(t_env *e)
{
	int	radio;

	radio = get_hardbass_sprite_nb(e);
	if (radio < 0)
		Mix_HaltChannel(CH_HARDBASS);
	else
	{
		if (e->sprite_dist[radio] > 0.01f)
			Mix_SetPosition(CH_HARDBASS, 0, (((double)(e->sprite_dist[radio]
			/ HARDBASS_RADIUS)) * 255));
		if (!Mix_Playing(CH_HARDBASS) && e->sprite_dist[radio]
			< HARDBASS_RADIUS)
			Mix_PlayChannel(CH_HARDBASS, e->sfx[((SDL_GetTicks() % 2) ? 9 : 10)]
			, -1);
	}
}
