/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   floor_casting.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/30 18:10:18 by almoraru          #+#    #+#             */
/*   Updated: 2019/06/23 17:37:38 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	prep_and_avoid_over_flow(t_env *e)
{
	e->dist_wall = e->perp_dist;
	e->dist_player = 0.0f;
	if (e->wall.d_end < 0)
		e->wall.d_end = WIN_HEIGHT;
}

void	prepare_floor_x_y(t_env *e)
{
	e->f.tex_x = (unsigned int)(e->f.x * e->floor_tex[0]->w)
		% e->floor_tex[0]->w;
	if (e->f.tex_x < 0)
		e->f.tex_x = 0;
	e->f.tex_y = (unsigned int)(e->f.y * e->floor_tex[0]->h)
		% e->floor_tex[0]->h;
	if (e->f.tex_y < 0)
		e->f.tex_y = 0;
}

void	draw_floor(t_env *e, int x)
{
	Uint32	*p;
	Uint32	color;
	int		y;

	prep_and_avoid_over_flow(e);
	y = e->wall.d_end - 1;
	while (++y < WIN_HEIGHT)
	{
		e->current_dist = WIN_HEIGHT / (2.0f * y - WIN_HEIGHT);
		e->f.weight = (e->current_dist - e->dist_player) /
						(e->dist_wall - e->dist_player);
		e->f.x = e->f.weight * e->f.x_wall + (1.0f - e->f.weight) * e->pos.x;
		e->f.y = e->f.weight * e->f.y_wall + (1.0f - e->f.weight) * e->pos.y;
		prepare_floor_x_y(e);
		p = e->floor_tex[0]->pixels;
		color = p[e->floor_tex[0]->w * e->f.tex_y + e->f.tex_x];
		e->screen_buf[y * WIN_WIDTH + x] = color;
		p = e->floor_tex[1]->pixels;
		color = p[e->floor_tex[0]->w * e->f.tex_y + e->f.tex_x];
		e->screen_buf[(WIN_HEIGHT - y) * WIN_WIDTH + x] = color;
	}
}

void	prepare_floor_casting(t_env *e)
{
	if (e->wall.side == 0 && e->ray.dir_x > 0)
	{
		e->f.x_wall = e->m.x;
		e->f.y_wall = e->m.y + e->wall.x;
	}
	else if (e->wall.side == 0 && e->ray.dir_x < 0)
	{
		e->f.x_wall = e->m.x + 1.0f;
		e->f.y_wall = e->m.y + e->wall.x;
	}
	else if (e->wall.side == 1 && e->ray.dir_y > 0)
	{
		e->f.x_wall = e->m.x + e->wall.x;
		e->f.y_wall = e->m.y;
	}
	else
	{
		e->f.x_wall = e->m.x + e->wall.x;
		e->f.y_wall = e->m.y + 1.0f;
	}
}
