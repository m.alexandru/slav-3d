/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/30 17:41:48 by almoraru          #+#    #+#             */
/*   Updated: 2019/06/23 19:23:06 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	init_audio2(t_env *e)
{
	e->sfx[6] = Mix_LoadWAV("sounds/blyat/blyat2.wav");
	(!e->sfx[6] ? ft_error((char*)Mix_GetError(), e) : 1);
	e->sfx[7] = Mix_LoadWAV("sounds/blyat/blyat3.wav");
	(!e->sfx[7] ? ft_error((char*)Mix_GetError(), e) : 1);
	e->sfx[8] = Mix_LoadWAV("sounds/blyat/greet1.wav");
	(!e->sfx[8] ? ft_error((char*)Mix_GetError(), e) : 1);
	e->sfx[9] = Mix_LoadWAV("sounds/music/radio1.wav");
	(!e->sfx[9] ? ft_error((char*)Mix_GetError(), e) : 1);
	e->sfx[10] = Mix_LoadWAV("sounds/music/radio2.wav");
	(!e->sfx[10] ? ft_error((char*)Mix_GetError(), e) : 1);
	Mix_AllocateChannels(NB_CHANNELS);
}

void	init_audio(t_env *e)
{
	if (Mix_OpenAudio(44100, AUDIO_S16SYS, 2, 2048))
		ft_error((char*)Mix_GetError(), e);
	e->bgm[0] = Mix_LoadMUS("sounds/music/banditradio.ogg");
	(!e->bgm[0] ? ft_error((char*)Mix_GetError(), e) : 1);
	e->sfx[0] = Mix_LoadWAV("sounds/steps.wav");
	(!e->sfx[0] ? ft_error((char*)Mix_GetError(), e) : 1);
	e->sfx[1] = Mix_LoadWAV("sounds/items/ak47_draw.wav");
	(!e->sfx[1] ? ft_error((char*)Mix_GetError(), e) : 1);
	e->sfx[2] = Mix_LoadWAV("sounds/items/ak47_draw2.wav");
	(!e->sfx[2] ? ft_error((char*)Mix_GetError(), e) : 1);
	e->sfx[3] = Mix_LoadWAV("sounds/items/vodka.wav");
	(!e->sfx[3] ? ft_error((char*)Mix_GetError(), e) : 1);
	e->sfx[4] = Mix_LoadWAV("sounds/items/ak47_pew.wav");
	(!e->sfx[4] ? ft_error((char*)Mix_GetError(), e) : 1);
	e->sfx[5] = Mix_LoadWAV("sounds/blyat/greet2.wav");
	(!e->sfx[5] ? ft_error((char*)Mix_GetError(), e) : 1);
	init_audio2(e);
}

void	init_textures(t_env *e)
{
	if (!(e->textures[0] = png2surf("textures/walls/wall1.png")))
		ft_error("Failed to load texture!", e);
	if (!(e->textures[1] = png2surf("textures/walls/wall2.png")))
		ft_error("Failed to load texture!", e);
	if (!(e->textures[2] = png2surf("textures/walls/wall3.png")))
		ft_error("Failed to load texture!", e);
	if (!(e->textures[3] = png2surf("textures/walls/wall5.png")))
		ft_error("Failed to load texture!", e);
	if (!(e->textures[4] = png2surf("textures/ui/ak.png")))
		ft_error("Failed to load texture!", e);
	if (!(e->textures[5] = png2surf("textures/sprites/vodka4.png")))
		ft_error("Failed to load texture!", e);
	if (!(e->floor_tex[0] = png2surf("textures/walls/floor.png")))
		ft_error("Failed to load texture!", e);
	if (!(e->floor_tex[1] = png2surf("textures/walls/ceil.png")))
		ft_error("Failed to load texture!", e);
	if (!(e->ui[0] = SDL_CreateTextureFromSurface(e->renderer, e->textures[4])))
		ft_error("Failed to load texture!", e);
	if (!(e->ui[1] = SDL_CreateTextureFromSurface(e->renderer, e->textures[5])))
		ft_error("Failed to load texture!", e);
}

int		init_win(char *title, unsigned int width, unsigned int height
				, t_env *e)
{
	if (e->win || e->renderer)
		return (FALSE);
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
		ft_error("Failed to initialize SDL", e);
	init_audio(e);
	if (!(e->win = SDL_CreateWindow(title, 0, 0, width, height
									, SDL_WINDOW_SHOWN)))
		ft_error("Failed to make window or renderer!", e);
	if (!(e->renderer = SDL_CreateRenderer(e->win, -1
									, SDL_RENDERER_SOFTWARE)))
		ft_error("Failed to make window or renderer!", e);
	SDL_SetWindowTitle(e->win, title);
	SDL_SetRenderDrawColor(e->renderer, 255, 255, 255, 255);
	SDL_RenderClear(e->renderer);
	SDL_RenderPresent(e->renderer);
	e->g.game = 1;
	return (TRUE);
}

int		ft_init(t_env *e)
{
	e->sp.u_div = 1;
	e->sp.v_div = 1;
	e->sp.v_move = 0.0f;
	e->managed_text = NULL;
	if (!init_win("SLAV 3D", WIN_WIDTH, WIN_HEIGHT, e))
		return (FALSE);
	if (!(e->screen_buf = create_texture(WIN_WIDTH, WIN_HEIGHT, e)))
		return (FALSE);
	init_textures(e);
	return (TRUE);
}
