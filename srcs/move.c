/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/03 10:39:00 by fdubois           #+#    #+#             */
/*   Updated: 2019/06/23 19:45:37 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	move_right_or_left(t_env *e, double old_dir_x
		, double old_plane_x, int i)
{
	if (i == 1)
	{
		old_dir_x = e->cam.dir_x;
		e->cam.dir_x = e->cam.dir_x * cos(-0.05f) - e->cam.dir_y * sin(-0.05f);
		e->cam.dir_y = old_dir_x * sin(-0.05f) + e->cam.dir_y * cos(-0.05f);
		old_plane_x = e->plane.x;
		e->plane.x = e->plane.x * cos(-0.05f) - e->plane.y * sin(-0.05f);
		e->plane.y = old_plane_x * sin(-0.05f) + e->plane.y * cos(-0.05f);
	}
	else
	{
		old_dir_x = e->cam.dir_x;
		e->cam.dir_x = e->cam.dir_x * cos(0.05f) - e->cam.dir_y * sin(0.05f);
		e->cam.dir_y = old_dir_x * sin(0.05f) + e->cam.dir_y * cos(0.05f);
		old_plane_x = e->plane.x;
		e->plane.x = e->plane.x * cos(0.05f) - e->plane.y * sin(0.05f);
		e->plane.y = old_plane_x * sin(0.05f) + e->plane.y * cos(0.05f);
	}
}

void	actual_move(t_env *e, int acc)
{
	if (!acc)
		e->speed = (e->speed ? e->speed / 2 : 0);
	else
	{
		e->speed = log10((acc > 0 ? acc : -acc)) * MAX_SPEED;
		if (acc < 0)
			e->speed *= -1;
	}
	if (e->map[(int)(e->pos.x + e->cam.dir_x * e->speed)][(int)(e->pos.y)] != W)
		e->pos.x += e->cam.dir_x * e->speed;
	if (e->map[(int)(e->pos.x)][(int)(e->pos.y + e->cam.dir_y * e->speed)] != W)
		e->pos.y += e->cam.dir_y * e->speed;
}

void	new_mvmt(t_env *e)
{
	if (e->pos.x < e->maph - 1 && e->pos.y < e->mapw - 1)
	{
		if (e->mv & M_F)
		{
			if (e->acc <= 0)
				e->acc = 1;
			else if (e->speed < MAX_SPEED)
				e->acc++;
		}
		else if (e->mv & M_B)
		{
			if (e->acc >= 0)
				e->acc = -1;
			else if (e->speed < MAX_SPEED)
				e->acc--;
		}
		else if (!(e->mv & (M_B | M_F)))
		{
			if (e->acc)
				(e->acc < 0 ? e->acc++ : e->acc--);
			else
				e->speed = 0;
		}
		actual_move(e, e->acc);
	}
}

int		is_mvmt_key(int key)
{
	return (key == SDLK_w || key == SDLK_s || key == SDLK_a
			|| key == SDLK_d || key == SDLK_q || key == SDLK_e);
}

void	move_it(t_env *e)
{
	if (!e->mv)
		Mix_FadeOutChannel(NB_CHANNELS - 1, 42);
	if (e->mv & M_L)
		move_right_or_left(e, 0, 0, 2);
	if (e->mv & M_R)
		move_right_or_left(e, 0, 0, 1);
	new_mvmt(e);
}
