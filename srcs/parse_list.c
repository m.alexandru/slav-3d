/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_list.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/01 15:28:52 by almoraru          #+#    #+#             */
/*   Updated: 2019/06/01 15:28:56 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

t_li	*new_node(signed char *str)
{
	t_li	*node;

	if (!(node = (t_li*)malloc(sizeof(t_li))))
		return (NULL);
	node->str = str;
	node->next = NULL;
	return (node);
}

void	add_node(t_li **list, t_li *node)
{
	t_li	*tmp;

	tmp = *list;
	if (!node)
		return ;
	if (!(*list))
		*list = node;
	else
	{
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = node;
	}
}

void	free_node(t_li **node)
{
	(*node)->next = NULL;
	ft_memdel((void**)node);
}

int		list_size(t_li *list)
{
	int	i;

	i = 0;
	if (list)
	{
		while (list)
		{
			list = list->next;
			i++;
		}
	}
	return (i);
}

void	free_list(t_li **list, size_t len)
{
	if ((*list))
	{
		free_list(&((*list)->next), len);
		free_node(list);
	}
}
