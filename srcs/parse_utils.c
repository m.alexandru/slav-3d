/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/02 17:42:18 by fdubois           #+#    #+#             */
/*   Updated: 2019/06/22 17:12:59 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int				ft_stris(signed char *str, char c)
{
	int	i;

	i = -1;
	while (str[++i])
		if (str[i] != c)
			return (0);
	return (1);
}

void			player_spawn_pos(t_env *e, int x)
{
	if (e->pos.x || e->pos.y)
		ft_error("invalid map: several start points in map.", e);
	else
	{
		e->pos.x = x + 0.5f;
		e->pos.y = e->maph + 0.5f;
	}
}

void			sprites_value(signed char *s)
{
	if (*s == 'B' || *s == 'C' || *s == 'D' || *s == 'E')
		if (*s == 'B' || *s == 'C')
			*s = (*s == 'B' ? B : C);
		else
			*s = (*s == 'D' ? D : E);
	else if (*s == 'R' || *s == 'S' || *s == 'T' || *s == 'U')
		if (*s == 'R' || *s == 'S')
			*s = (*s == 'R' ? R : S);
		else
			*s = (*s == 'T' ? T : U);
	else if (*s == 'V' || *s == 'X' || *s == 'Y' || *s == 'Z')
	{
		if (*s == 'V' || *s == 'X')
			*s = (*s == 'V' ? V : X);
		else
			*s = (*s == 'Y' ? Y : Z);
	}
}

void			get_map_value(signed char *s)
{
	if (*s == 'P' || *s == 'F' || *s == 'W' || *s == 'A')
		if (*s == 'P' || *s == 'F')
			*s = (*s == 'F' ? F : P);
		else
			*s = (*s == 'W' ? W : A);
	else if (*s == 'G' || *s == 'H' || *s == 'I' || *s == 'J')
		if (*s == 'G' || *s == 'H')
			*s = (*s == 'G' ? G : H);
		else
			*s = (*s == 'I' ? I : J);
	else
		sprites_value(s);
}
