/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   placement.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/03 12:11:22 by fdubois           #+#    #+#             */
/*   Updated: 2019/06/29 16:12:10 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int		put_sprite_tex(t_env *e, int i, int x, int y)
{
	i = put_sprite_tex1(e, i, x, y);
	i = put_sprite_tex2(e, i, x, y);
	i = put_sprite_tex3(e, i, x, y);
	i = put_sprite_tex4(e, i, x, y);
	if (e->map[y][x] == Y)
	{
		if (!(e->sprite_tex[i] = png2surf("textures/sprites/vodka3.png")))
			ft_error("Failed to load texture!", e);
		e->s[i].flags = S_VOD;
		i++;
	}
	else if (e->map[y][x] == Z)
	{
		if (!(e->sprite_tex[i] = png2surf("textures/sprites/vodka4.png")))
			ft_error("Failed to load texture!", e);
		e->s[i].flags = S_VOD;
		i++;
	}
	return (i);
}

void	get_sprite_pos(t_env *e)
{
	int y;
	int x;
	int i;

	y = -1;
	i = 0;
	while (++y < e->maph)
	{
		x = -1;
		while (++x < e->mapw)
		{
			if (e->map[y][x] > W && i < e->sprite_nb)
			{
				e->s[i].x = y + 0.5f;
				e->s[i].y = x + 0.5f;
				i = put_sprite_tex(e, i, x, y);
			}
		}
	}
}

void	get_nb_sprites(t_env *e)
{
	int y;
	int x;
	int pc;

	y = -1;
	pc = 0;
	while (++y < e->maph)
	{
		x = -1;
		while (++x < e->mapw)
		{
			if (e->map[y][x] == P)
			{
				e->pos.x = (double)y + 0.5f;
				e->pos.y = (double)x + 0.5f;
				pc++;
			}
		}
	}
	if (pc != 1)
		ft_error("No players? What is this?", e);
}

void	check_last_wall(t_env *e)
{
	int x;

	x = -1;
	while (++x < e->mapw)
	{
		if (e->map[e->maph - 1][x] == F)
			ft_error("Invalid map! Lacks some walls!", e);
	}
}
