/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   png_filters.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/04 15:44:58 by fdubois           #+#    #+#             */
/*   Updated: 2019/06/22 17:13:26 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "png.h"

void			sub_unfilter(unsigned char *str, uint32_t i, t_img *img)
{
	int j;

	j = -1;
	while (++j < img->bpp)
		if (i > img->bpp && (i % img->linesize != 1))
			str[(i + j)] = (str[(i + j)] + str[(i + j - img->bpp - ((i % img->
linesize == 1) ? 1 : 0))]) % 256;
}

void			up_unfilter(unsigned char *str, uint32_t i, t_img *img)
{
	int j;

	j = -1;
	while (++j < img->bpp)
		if (i > img->linesize)
			str[(i + j)] = (str[(i + j)] + str[(i + j - img->linesize)]) % 256;
}

void			average_unfilter(unsigned char *str, uint32_t i, t_img *img)
{
	int j;

	j = -1;
	while (++j < img->bpp)
		str[(i + j)] = (str[(i + j)] + (((i % img->linesize != 1 ? str[(
i + j - img->bpp)] : 0) + (i > img->linesize ? str[(i + j - img->linesize)
] : 0)) / 2)) % 256;
}

static uint8_t	paeth_predict(uint8_t a, uint8_t b, uint8_t c)
{
	int p;
	int pa;
	int pb;
	int pc;

	p = a + b - c;
	pa = abs(p - a);
	pb = abs(p - b);
	pc = abs(p - c);
	if (pa <= pb && pa <= pc)
		return (a);
	return (pb <= pc ? b : c);
}

void			paeth_unfilter(unsigned char *str, uint32_t i, t_img *img)
{
	int j;

	j = -1;
	while (++j < img->bpp)
	{
		if (i > img->linesize && (i % img->linesize == 1))
			str[(i + j)] = (str[(i + j)] + paeth_predict(0, str[(i + j - img->
linesize)], 0)) % 256;
		else if (i > img->linesize && (i % img->linesize > 1))
			str[(i + j)] = (str[(i + j)] + paeth_predict(str[(i + j - img->bpp)]
, str[(i + j - img->linesize)], str[(i + j - img->bpp - img->linesize)])) % 256;
		else if (i > img->bpp && i < img->linesize)
			str[(i + j)] = (str[(i + j)] + paeth_predict(str[
(i + j - img->bpp)], 0, 0)) % 256;
	}
}
