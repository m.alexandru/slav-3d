/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   put_sprite_tex.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/22 18:20:00 by almoraru          #+#    #+#             */
/*   Updated: 2019/06/23 18:39:48 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int		put_sprite_tex4(t_env *e, int i, int x, int y)
{
	if (e->map[y][x] == I)
	{
		if (!(e->sprite_tex[i] = png2surf("textures/sprites/gopnik5.png")))
			ft_error("Failed to load texture!", e);
		e->s[i].flags = S_PNJ;
		i++;
	}
	else if (e->map[y][x] == J)
	{
		if (!(e->sprite_tex[i] = png2surf("textures/sprites/gopnikboss.png")))
			ft_error("Failed to load texture!", e);
		e->s[i].flags = S_PNJ;
		i++;
	}
	else if (e->map[y][x] == V)
	{
		if (!(e->sprite_tex[i] = png2surf("textures/sprites/vodka.png")))
			ft_error("Failed to load texture!", e);
		e->s[i].flags = S_VOD;
		i++;
	}
	return (i);
}

int		put_sprite_tex3(t_env *e, int i, int x, int y)
{
	if (e->map[y][x] == G)
	{
		if (!(e->sprite_tex[i] = png2surf("textures/sprites/gopnik.png")))
			ft_error("Failed to load texture!", e);
		e->s[i].flags = S_PNJ;
		i++;
	}
	else if (e->map[y][x] == H)
	{
		if (!(e->sprite_tex[i] = png2surf("textures/sprites/gopnik2.png")))
			ft_error("Failed to load texture!", e);
		e->s[i].flags = S_PNJ;
		i++;
	}
	else if (e->map[y][x] == X)
	{
		if (!(e->sprite_tex[i] = png2surf("textures/sprites/vodka2.png")))
			ft_error("Failed to load texture!", e);
		e->s[i].flags = S_VOD;
		i++;
	}
	return (i);
}

int		put_sprite_tex2(t_env *e, int i, int x, int y)
{
	if (e->map[y][x] == D)
	{
		if (!(e->sprite_tex[i] = png2surf("textures/pics/pillar.png")))
			ft_error("Failed to load texture!", e);
		i++;
	}
	else if (e->map[y][x] == E)
	{
		if (!(e->sprite_tex[i] = png2surf("textures/sprites/radio2.png")))
			ft_error("Failed to load texture!", e);
		e->s[i].flags = S_RA2;
		e->inv |= I_RA2;
		i++;
	}
	return (i);
}

int		put_sprite_tex1(t_env *e, int i, int x, int y)
{
	if (e->map[y][x] == A)
	{
		if (!(e->sprite_tex[i] = png2surf("textures/sprites/ak.png")))
			ft_error("Failed to load texture!", e);
		e->s[i].flags = S_AKM;
		i++;
	}
	else if (e->map[y][x] == B)
	{
		if (!(e->sprite_tex[i] = png2surf("textures/pics/barrel.png")))
			ft_error("Failed to load texture!", e);
		i++;
	}
	else if (e->map[y][x] == C)
	{
		if (!(e->sprite_tex[i] = png2surf("textures/sprites/radio.png")))
			ft_error("Failed to load texture!", e);
		e->s[i].flags = S_RA1;
		e->inv |= I_RA1;
		i++;
	}
	return (i);
}
