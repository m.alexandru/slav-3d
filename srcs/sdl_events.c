/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sdl_events.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/28 11:41:11 by almoraru          #+#    #+#             */
/*   Updated: 2019/06/27 12:20:13 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	wasd(t_env *e, char mode)
{
	if (e->event.key.keysym.sym == SDLK_w)
		e->mv = (mode ? e->mv | M_F : e->mv & ~M_F);
	else if (e->event.key.keysym.sym == SDLK_s)
		e->mv = (mode ? e->mv | M_B : e->mv & ~M_B);
	else if (e->event.key.keysym.sym == SDLK_a)
		e->mv = (mode ? e->mv | M_L : e->mv & ~M_L);
	else if (e->event.key.keysym.sym == SDLK_d)
		e->mv = (mode ? e->mv | M_R : e->mv & ~M_R);
	else if (e->event.key.keysym.sym == SDLK_q)
		e->mv = (mode ? e->mv | M_SL : e->mv & ~M_SL);
	else if (e->event.key.keysym.sym == SDLK_e)
		e->mv = (mode ? e->mv | M_SR : e->mv & ~M_SR);
}

void	mouse_turn(t_env *e, double old_dir_x, double old_plane_x, Sint32 x)
{
	double dir;

	dir = ((double)x) / (WIN_WIDTH / 2.0f);
	old_dir_x = e->cam.dir_x;
	e->cam.dir_x = e->cam.dir_x * cos(dir) - e->cam.dir_y * sin(dir);
	e->cam.dir_y = old_dir_x * sin(dir) + e->cam.dir_y * cos(dir);
	old_plane_x = e->plane.x;
	e->plane.x = e->plane.x * cos(dir) - e->plane.y * sin(dir);
	e->plane.y = old_plane_x * sin(dir) + e->plane.y * cos(dir);
}

void	handle_sdl_events(t_env *e)
{
	while ((SDL_PollEvent(&e->event) != 0))
	{
		if (e->event.type == SDL_MOUSEMOTION)
			mouse_turn(e, 0, 0, -e->event.motion.xrel);
		else if (e->event.type == SDL_KEYUP
			&& is_mvmt_key(e->event.key.keysym.sym))
			wasd(e, 0);
		else if (e->event.type == SDL_KEYDOWN)
		{
			if (is_mvmt_key(e->event.key.keysym.sym))
			{
				if (!Mix_Playing(NB_CHANNELS - 1))
					Mix_PlayChannel(NB_CHANNELS - 1, e->sfx[0], 0);
				wasd(e, 1);
			}
			else if (e->event.key.keysym.sym == SDLK_ESCAPE)
				free_everything_and_quit_normal(e);
		}
		else if (e->inv & I_AKM && e->event.type == SDL_MOUSEBUTTONDOWN)
			Mix_PlayChannel(-1, e->sfx[4], 0);
		else if (e->event.type == SDL_QUIT)
			free_everything_and_quit_normal(e);
	}
}
