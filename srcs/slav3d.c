/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   slav3d.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/30 17:55:17 by almoraru          #+#    #+#             */
/*   Updated: 2019/06/23 17:48:43 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	slav_add_list(Uint32 *data, t_texture *tex, t_env *e)
{
	*((t_texture**)data) = tex;
	tex->pixel_data = ((t_texture**)data) + 1;
	if (e->managed_text)
	{
		tex->next = e->managed_text;
		e->managed_text->prev = tex;
	}
	e->managed_text = tex;
}

void	*create_texture(unsigned int width, unsigned int height, t_env *e)
{
	Uint32		*data;
	t_texture	*tex;

	if (!width || !height || !e->renderer)
		ft_error("SDL hasn't been initliazed yet!", e);
	tex = (t_texture *)malloc(sizeof(t_texture));
	tex->pitch = width * sizeof(Uint32);
	tex->next = NULL;
	tex->prev = NULL;
	tex->tag = TEXTURE_ADDR;
	SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1");
	if (!(tex->texture = SDL_CreateTexture(e->renderer, SDL_PIXELFORMAT_ARGB8888
										, SDL_TEXTUREACCESS_STREAMING
										, width, height)))
	{
		free(tex);
		ft_error("Failed to make texture!", e);
	}
	data = (Uint32 *)malloc((sizeof(Uint32) * width * height)
							+ sizeof(t_texture *));
	slav_add_list(data, tex, e);
	return (tex->pixel_data);
}

int		destroy_texture(void *p, t_env *e)
{
	t_texture *t;

	t = *(((t_texture**)p) - 1);
	if (t->tag != TEXTURE_ADDR)
	{
		write(1, "Not a valid texture pointer", 28);
		return (0);
	}
	t->tag = 0;
	free(((t_texture**)p) - 1);
	SDL_DestroyTexture(t->texture);
	if (t->prev)
		t->prev->next = t->next;
	if (t->next)
		t->next->prev = t->prev;
	if (e->managed_text == t)
		e->managed_text = t->next;
	free(t);
	return (1);
}

void	render_hud(t_env *e)
{
	int	i;

	i = -1;
	if (e->inv & I_AKM)
	{
		SDL_RenderCopy(e->renderer, e->ui[0], NULL,
			&(SDL_Rect){.x = WIN_WIDTH / 2, .y = WIN_HEIGHT
				/ 2, .w = WIN_WIDTH / 2, .h = WIN_HEIGHT / 2});
		SDL_RenderPresent(e->renderer);
	}
	if (e->inv & I_VOD)
		while (++i < e->vodka && i < (WIN_WIDTH / 60))
			SDL_RenderCopy(e->renderer, e->ui[1], NULL, &(SDL_Rect){.x = (i + 1)
	* 60, .y = 3 * WIN_HEIGHT / 4, .w = 60, .h = WIN_HEIGHT / 6});
}

void	slav_draw_full_screen_texture(t_env *e, void *texture)
{
	t_texture *t;

	if (e->win == NULL || e->renderer == NULL)
		ft_error("SDL window has not beed initialized yet", e);
	t = *(((t_texture**)texture) - 1);
	if (t->tag != TEXTURE_ADDR)
		ft_error("Not a valid texture pointer", e);
	SDL_UpdateTexture(t->texture, NULL, t->pixel_data, t->pitch);
	SDL_RenderClear(e->renderer);
	SDL_RenderCopy(e->renderer, t->texture, NULL, NULL);
	render_hud(e);
	SDL_RenderPresent(e->renderer);
}
