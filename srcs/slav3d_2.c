/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   slav3d_2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/30 17:59:49 by almoraru          #+#    #+#             */
/*   Updated: 2019/05/30 18:00:55 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	clear_renderer(t_env *e)
{
	SDL_RenderClear(e->renderer);
}

void	present_renderer(t_env *e)
{
	SDL_RenderPresent(e->renderer);
}
