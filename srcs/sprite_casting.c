/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sprite_casting.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/30 18:25:41 by almoraru          #+#    #+#             */
/*   Updated: 2019/06/22 18:27:45 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	invert_matrix(t_env *e)
{
	e->sp.inv_det = 1.0f / (e->plane.x * e->cam.dir_y
							- e->cam.dir_x * e->plane.y);
	e->sp.trans_x = e->sp.inv_det * (e->cam.dir_y * e->sp.x
							- e->cam.dir_x * e->sp.y);
	e->sp.trans_y = e->sp.inv_det * (-e->plane.y * e->sp.x
							+ e->plane.x * e->sp.y);
}

void	draw_sprites(t_env *e, int stripe, int i)
{
	int		y;
	Uint32	color;
	Uint32	*p;

	y = e->sp.d_start_y - 1;
	while (++y < e->sp.d_end_y)
	{
		e->sp.d = (y - e->sp.v_move_screen) * 256 - WIN_HEIGHT * 128
			+ e->sp.height * 128;
		e->sp.tex_y = ((e->sp.d
			* e->sprite_tex[e->sprite_order[i]]->h) / e->sp.height) / 256;
		p = e->sprite_tex[e->sprite_order[i]]->pixels;
		if (e->sp.tex_y < 0)
			e->sp.tex_y = 0;
		color = p[e->sprite_tex[e->sprite_order[i]]->w
				* e->sp.tex_y + e->sp.tex_x];
		if ((color & 0x00FFFFFF) && color != 0x00FFFFFF)
			e->screen_buf[y * WIN_WIDTH + stripe] = color;
	}
}

void	draw_sprites_width(t_env *e, int i)
{
	int stripe;

	e->sp.width = abs((int)(WIN_HEIGHT / (e->sp.trans_y))) / e->sp.u_div;
	e->sp.d_start_x = -(e->sp.width) / 2 + e->sp.screen_x;
	if (e->sp.d_start_x < 0)
		e->sp.d_start_x = 0;
	e->sp.d_end_x = e->sp.width / 2 + e->sp.screen_x;
	if (e->sp.d_end_x >= WIN_WIDTH)
		e->sp.d_end_x = WIN_WIDTH - 1;
	stripe = e->sp.d_start_x - 1;
	while (++stripe < e->sp.d_end_x)
	{
		e->sp.tex_x = (int)(256 * (stripe - (-(e->sp.width) / 2
						+ e->sp.screen_x))
		* e->sprite_tex[e->sprite_order[i]]->w / e->sp.width) / 256;
		if (e->sp.tex_x < 0)
			e->sp.tex_x = 0;
		if (e->sp.trans_y > 0 && stripe > 0 && stripe < WIN_WIDTH
			&& e->sp.trans_y < e->z_buf[stripe])
			draw_sprites(e, stripe, i);
	}
}

void	draw_sprites_height(t_env *e, int i)
{
	e->sp.v_move_screen = (int)(e->sp.v_move / e->sp.trans_y);
	e->sp.height = abs((int)(WIN_HEIGHT / (e->sp.trans_y))) / e->sp.v_div;
	e->sp.d_start_y = -(e->sp.height) / 2 + WIN_HEIGHT / 2
						+ e->sp.v_move_screen;
	if (e->sp.d_start_y < 0)
		e->sp.d_start_y = 0;
	e->sp.d_end_y = e->sp.height / 2 + WIN_HEIGHT / 2 + e->sp.v_move_screen;
	if (e->sp.d_end_y >= WIN_HEIGHT)
		e->sp.d_end_y = WIN_HEIGHT - 1;
	draw_sprites_width(e, i);
}

void	sort_and_prep_sprites(t_env *e)
{
	int i;

	i = -1;
	while (++i < e->sprite_nb)
	{
		e->sprite_order[i] = i;
		e->sprite_dist[i] = ((e->pos.x - e->s[i].x) * (e->pos.x - e->s[i].x)
							+ (e->pos.y - e->s[i].y) * (e->pos.y - e->s[i].y));
	}
	i = -1;
	comb_sort(e->sprite_order, e->sprite_dist, e->sprite_nb);
	while (++i < e->sprite_nb)
	{
		e->sp.x = e->s[e->sprite_order[i]].x - e->pos.x;
		e->sp.y = e->s[e->sprite_order[i]].y - e->pos.y;
		invert_matrix(e);
		e->sp.screen_x = (int)((WIN_WIDTH / 2)
						* (1 + e->sp.trans_x / e->sp.trans_y));
		draw_sprites_height(e, i);
	}
}
