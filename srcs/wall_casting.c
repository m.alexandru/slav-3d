/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wall_casting.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/30 18:10:28 by almoraru          #+#    #+#             */
/*   Updated: 2019/06/22 18:26:03 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

Uint32	just_draw(t_env *e, Uint32 *p, Uint32 color)
{
	if (e->wall.side == 0 && e->ray.dir_x > 0)
	{
		p = e->textures[0]->pixels;
		color = p[e->textures[0]->w * e->wall.tex_y + e->wall.tex_x];
	}
	else if (e->wall.side == 0 && e->ray.dir_x < 0)
	{
		p = e->textures[1]->pixels;
		color = p[e->textures[1]->w * e->wall.tex_y + e->wall.tex_x];
	}
	else if (e->wall.side == 1 && e->ray.dir_y > 0)
	{
		p = e->textures[2]->pixels;
		color = p[e->textures[2]->w * e->wall.tex_y + e->wall.tex_x];
	}
	else if (e->wall.side == 1 && e->ray.dir_y < 0)
	{
		p = e->textures[3]->pixels;
		color = p[e->textures[3]->w * e->wall.tex_y + e->wall.tex_x];
	}
	return (color);
}

void	draw_walls(t_env *e, int x)
{
	Uint32	*p;
	Uint32	color;
	int		y;
	int		i;

	i = 0;
	y = e->wall.d_start - 1;
	p = NULL;
	color = 0;
	if (e->wall.side == 0 && e->ray.dir_x < 0)
		i = 1;
	else if (e->wall.side == 1)
		i = (e->ray.dir_y > 0 ? 2 : 3);
	while (++y < e->wall.d_end)
	{
		e->wall.d = y * 256 - WIN_HEIGHT * 128 + e->wall.line_height * 128;
		e->wall.tex_y = ((e->wall.d * e->textures[i]->h)
						/ e->wall.line_height) / 256;
		if (e->wall.tex_y < 0 || e->wall.tex_y > e->textures[i]->pitch
													* e->textures[i]->h)
			e->wall.tex_y = 0;
		color = just_draw(e, p, color);
		e->screen_buf[y * WIN_WIDTH + x] = color;
	}
	e->z_buf[x] = e->perp_dist;
}

void	prepare_draw_2(t_env *e)
{
	e->wall.tex_x = (int)(e->wall.x * (double)(e->textures[e->tex_num]->w));
	if (e->wall.side == 0 && e->ray.dir_x > 0)
		e->wall.tex_x = e->textures[0]->w - e->wall.tex_x - 1;
	else if (e->wall.side == 1 && e->ray.dir_y < 0)
		e->wall.tex_x = e->textures[1]->w - e->wall.tex_x - 1;
}

void	prepare_draw(t_env *e)
{
	if (e->wall.side == 0)
		e->perp_dist = (e->m.x - e->pos.x + (1.0f - e->ray.step_x) / 2.0f)
		/ e->ray.dir_x;
	else
		e->perp_dist = (e->m.y - e->pos.y + (1.0f - e->ray.step_y) / 2.0f)
		/ e->ray.dir_y;
	e->wall.line_height = (int)(WIN_HEIGHT / e->perp_dist);
	e->wall.d_start = -e->wall.line_height / 2 + WIN_HEIGHT / 2;
	if (e->wall.d_start < 0)
		e->wall.d_start = 0;
	e->wall.d_end = e->wall.line_height / 2 + WIN_HEIGHT / 2;
	if (e->wall.d_end >= WIN_HEIGHT)
		e->wall.d_end = WIN_HEIGHT - 1;
	e->tex_num = e->map[e->m.x][e->m.y] - 1;
	if (e->wall.side == 0)
		e->wall.x = e->pos.y + e->perp_dist * e->ray.dir_y;
	else
		e->wall.x = e->pos.x + e->perp_dist * e->ray.dir_x;
	e->wall.x -= floor((e->wall.x));
	prepare_draw_2(e);
}
